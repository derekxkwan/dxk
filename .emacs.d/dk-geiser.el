(setq geiser-repl-read-only-prompt-p nil)
(setq geiser-racket--prompt-regexp "<pkgs>.*> \\|\\(mzscheme\\|racket\\)@[^ ]*> |->")